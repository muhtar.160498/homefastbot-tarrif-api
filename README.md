# Homefastbot-tarrif-api



## Getting started

| Endpoint | Method | Description |	Request Parameters |	Request Body | Response Body
| ------ | ------ | ------ | ------ | ------ | ------ |
|    /api/tariff   |   get     | 	Get a list of tariff | -                | -                 | result:ITariff[] |
|    /api/tariff    |   post    |   Create new tariff    | -                |Tariff json object      | savedTariff:ITariff| 
|  /api/tariff/:id  |   patch   |   Update tariff        | tariffId (string)|Updated tariff data| updatedTariff:ITariff|
|  /api/tariff/:id  |   delete  |   Delete tariff        | tariffId (string)|-                  | -                 |


interface ITariff extends Document { 

    name: string
    price: number
    qrLink: string
    maxFiltersCount: number
    isActive: boolean
    isArchive: boolean
    
}

const TariffSchema = new Schema<ITariff>(

    {

        name: {
            type: String,
            required: true,
        },
        price: {
            type: Number,
            required: true,
            min: 0,
        },
        qrLink: {
            type: String,
            default: '',
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        isArchive: {
            type: Boolean,
            default: false,
        },
        maxFiltersCount: {
            type: Number,
            default: 1,
        },
    },

    {
        timestamps: true,
        collection: COLLECTION_NAME,
    }
    
)
