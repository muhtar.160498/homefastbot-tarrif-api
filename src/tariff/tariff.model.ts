import { Document, model, Schema } from 'mongoose'

const COLLECTION_NAME = 'Tariff'

export interface ITariff extends Document {
    name: string
    price: number
    qrLink: string
    maxFiltersCount: number
    isActive: boolean
    isArchive: boolean
}

const TariffSchema = new Schema<ITariff>(
    {
        name: {
            type: String,
            required: true,
        },
        price: {
            type: Number,
            required: true,
            min: 0,
        },
        qrLink: {
            type: String,
            default: '',
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        isArchive: {
            type: Boolean,
            default: false,
        },
        maxFiltersCount: {
            type: Number,
            default: 1,
        },
    },
    {
        timestamps: true,
        collection: COLLECTION_NAME,
    }
)

export const TariffModel = model<ITariff>(COLLECTION_NAME, TariffSchema, COLLECTION_NAME)
