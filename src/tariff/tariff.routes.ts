import { Router } from 'express'
import { AuthMiddleware } from '../auth/auth.middleware'
import { TariffController } from './tariff.controller'

const router = Router()

router.get('/', TariffController.find)
router.post('/', TariffController.save)
router.patch('/:id', TariffController.update);
router.delete('/:id', TariffController.delete);

export default router
