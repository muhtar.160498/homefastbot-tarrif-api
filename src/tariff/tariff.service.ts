import { Types } from 'mongoose'
import { ITariff, TariffModel } from './tariff.model'

export class TariffService {
    public static async findOne(id: Types.ObjectId): Promise<ITariff | null> {
        const tariff = await TariffModel.findOne({ _id: id })

        return tariff
    }

    public static async find(): Promise<ITariff[]> {
        return await TariffModel.find()
    }

    public static async save(tariffData: ITariff): Promise<ITariff> {
        const newTariff = new TariffModel(tariffData);
        return await newTariff.save();
    }

    public static async update(id: Types.ObjectId, tariffData: ITariff): Promise<ITariff | null> {
        const updatedTariff = await TariffModel.findByIdAndUpdate(id, tariffData, { new: true });
        return updatedTariff;
    }

    public static async delete(id: Types.ObjectId): Promise<void> {
        await TariffModel.findByIdAndDelete(id);
    }
}
