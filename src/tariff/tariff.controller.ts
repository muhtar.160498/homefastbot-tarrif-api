import { Request, Response } from 'express'
import { TariffService } from './tariff.service'
import HTTP_STATUS_CODE from 'http-status-codes'
import { Types } from 'mongoose'

export class TariffController {
    public static async find(_req: Request, res: Response) {
        const result = await TariffService.find()

        res.status(HTTP_STATUS_CODE.OK).send(result)
    }

    public static async save(_req: Request, res: Response) {
        console.log(_req.body);
        try {
          const tariffData = _req.body; 
          const savedTariff = await TariffService.save(tariffData);
    
          res.status(HTTP_STATUS_CODE.CREATED).send(savedTariff);
        } catch (error) {
          console.error('Error saving tariff:', error);
          res.status(HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR).send('Error saving tariff');
        }
    }

    public static async update(req: Request, res: Response) {
        try {
            const tariffId = new Types.ObjectId(req.params.id);
            const tariffData = req.body;
            const updatedTariff = await TariffService.update(tariffId, tariffData);
            if (!updatedTariff) {
                return res.status(HTTP_STATUS_CODE.NOT_FOUND).send('Tariff not found');
            }
            res.status(HTTP_STATUS_CODE.OK).send(updatedTariff);
        } catch (error) {
            console.error('Error updating tariff:', error);
            res.status(HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR).send('Error updating tariff');
        }
    }


    public static async delete(req: Request, res: Response) {
        try {
            const tariffId = new Types.ObjectId(req.params.id);
            await TariffService.delete(tariffId);
            res.status(HTTP_STATUS_CODE.NO_CONTENT).send();
        } catch (error) {
            console.error('Error deleting tariff:', error);
            res.status(HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR).send('Error deleting tariff');
        }
    }
}
